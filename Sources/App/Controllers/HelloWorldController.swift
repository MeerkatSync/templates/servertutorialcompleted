import Vapor

final class HelloWorldController {
    /// Returns a list of all `Todo`s.
    func index(_ req: Request) throws -> String {
        return HelloWorld.hello
    }
}
