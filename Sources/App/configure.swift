import MeerkatServerProvider

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    // Register providers first
    let meerkatProvider = MeerkatServerProvider(userType: PrivateUser.self)
    try services.register(meerkatProvider)
    try services.register(AuthenticationProvider())

    let scheme = [Note.self, NotesFolder.self].scheme
    services.register(scheme)

    services.register(EmptyNotificator.self)

    // Register routes to the router
    let router = EngineRouter.default()
    meerkatProvider.setUpRoutes(router)
    try routes(router)
    services.register(router, as: Router.self)

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    // insert database configuration
    let hostname = // maybe "127.0.0.1"?
    let port = // maybe 3306?
    let username = // maybe "root"?
    let password: String? = // maybe nil?
    let database =

    let config = MySQLDatabaseConfig(hostname: hostname, port: port, username: username, password: password, database: database)
    let db = MySQLDatabase(config: config)
    var databases = DatabasesConfig()
    databases.add(database: db, as: .mysql)
    services.register(databases)

    // insert migrations
    var migrations = MigrationConfig()
    PrivateUser.defaultDatabase = .mysql
    UserToken.defaultDatabase = .mysql
    migrations.add(model: PrivateUser.self, database: .mysql)
    migrations.add(model: UserToken.self, database: .mysql)
    meerkatProvider.setUpMigration(&migrations) // add inner model's migrations
    services.register(migrations)
}
