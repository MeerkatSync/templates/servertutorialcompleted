import MeerkatServerProvider

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let helloController = HelloWorldController()
    router.get(use: helloController.index)

    let userController = DefaultUserController(builder: UserBuilder())
    userController.register(on: router)

    let groupInfoController = PublicGroupInfoController(userIdKeyPath: \PrivateUser.username) { user, role, groupId in
        PublicGroupUser(username: user.username, role: role)
    }
    groupInfoController.register(on: router)
}
