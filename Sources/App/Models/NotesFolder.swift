//
//  NotesFolder.swift
//  
//
//  Created by Filip Klembara on 05/05/2020.
//

struct NotesFolder: NotesFolderScheme {
    let title: String = ""

    let notes: Notes = []

    typealias Notes = [Note]
}
