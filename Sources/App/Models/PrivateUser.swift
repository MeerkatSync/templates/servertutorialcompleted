//
//  PrivateUser.swift
//  
//
//  Created by Filip Klembara on 05/05/2020.
//

import MeerkatServerProvider // 1

final class PrivateUser: MySQLModel, PrivateUserTable { // 2
    var id: Int?
    var username: String
    var passwordHash: String
    var syncId: String = UUID().uuidString

    init(username: String, password: String) {
        self.username = username
        self.passwordHash = password
    }
}

extension PrivateUser: TokenAuthenticatable { // 3
    typealias TokenType = UserToken
}

extension PrivateUser: Migration { } // 4
