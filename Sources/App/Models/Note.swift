//
//  Note.swift
//  
//
//  Created by Filip Klembara on 05/05/2020.
//

import Foundation

struct Note: NoteScheme {
    let title = ""

    let text = ""

    let created = Date()
}
