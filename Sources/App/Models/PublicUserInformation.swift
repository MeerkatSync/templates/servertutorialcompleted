//
//  PublicUserInformation.swift
//  
//
//  Created by Filip Klembara on 05/05/2020.
//

import MeerkatServerProvider

struct PublicUserInformation: Content {
    let username: String
    let password: String
}
