//
//  UserToken.swift
//  
//
//  Created by Filip Klembara on 05/05/2020.
//

import MeerkatServerProvider

struct UserToken: MySQLModel {
    var id: Int?
    var token: String   // 1
    var userID: PrivateUser.ID // 2
}

extension UserToken: Token {
    typealias UserType = PrivateUser // 3

    static var tokenKey: WritableKeyPath<UserToken, String> {
        return \.token  // 4
    }

    static var userIDKey: WritableKeyPath<UserToken, PrivateUser.ID> {
        return \.userID  // 5
    }
}

extension UserToken: Migration { }
