//
//  UserBuidler.swift
//
//
//  Created by Filip Klembara on 05/05/2020.
//

import MeerkatServerProvider

struct UserBuilder: UserBuilderProtocol {
    typealias UserTable = PrivateUser
    typealias PublicUser = PublicUserInformation
    typealias UserTokenAuth = PublicUserInformation // 1
    typealias UserDevice = DefaultDevice            // 2

    let userTableId: KeyPath<PrivateUser, String> = \.username // 3

    func createUser(from user: PublicUserInformation, on container: Container) throws -> EventLoopFuture<PrivateUser> {
        // 1
        let password = try container.make(BCryptDigest.self).hash(user.password)
        // 2
        let newUser = PrivateUser(username: user.username, password: password)
        // 3
        let userNotExists = container.withPooledConnection(to: .mysql) { conn -> Future<Void> in
            PrivateUser.query(on: conn)
                .filter(\.username == newUser.username)
                .first()
                .map { optU -> Void in
                    if let u = optU {
                        // 4
                        throw DBWrapperError.userAlreadyExists(id: u.username)
                    }

            }
        }
        // 5
        let save = userNotExists.then {
            container.withPooledConnection(to: .mysql) {
                newUser.save(on: $0)
            }
        }
        return save
    }

    func createToken(for user: PublicUserInformation, on container: Container) -> EventLoopFuture<UserToken> {
        // 1
        let getUser = container.withPooledConnection(to: .mysql) { conn in
            PrivateUser.query(on: conn)
                .filter(\.username == user.username)
                .first()
                .unwrap(or: DBWrapperError.unknownUser(id: user.username)) }
        // 2
        let isPasswordValid = getUser.map(to: Void.self) {
            // 3
            let check = try container.make(BCryptDigest.self)
                .verify(user.password, created: $0.passwordHash)
            // 4
            guard check else { throw Abort(.unauthorized) }
            return ()
        }
        // 5
        return isPasswordValid.transform(to: getUser).flatMap {
            try self.createToken(for: $0, on: container)
        }
    }

    // 6
    func createToken(for user: PrivateUser, on container: Container) throws -> EventLoopFuture<UserToken> {
        container.withPooledConnection(to: .mysql) { conn in
            UserToken(token: self.randomString(length: 128), userID: user.id!).save(on: conn)
        }
    }
}
