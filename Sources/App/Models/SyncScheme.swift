//
//  SyncScheme.swift
//  
//
//  Created by Filip Klembara on 05/05/2020.
//

import MeerkatSchemeDescriptor
import Foundation

public protocol NoteScheme: SchemeDescribable {
    var title: String { get }
    var text: String { get }
    var created: Date { get }
}

public protocol NotesFolderScheme: SchemeDescribable {
    var title: String { get }
    var notes: Notes { get }

    associatedtype Notes: Collection where Notes.Element: NoteScheme
}
