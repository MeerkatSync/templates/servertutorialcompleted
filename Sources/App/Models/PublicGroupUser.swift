//
//  PublicGroupUser.swift
//  
//
//  Created by Filip Klembara on 05/05/2020.
//

import MeerkatServerProvider

struct PublicGroupUser: Content {
    let username: String
    let role: Role
}
