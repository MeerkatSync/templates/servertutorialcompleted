// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "ServerTemplate",
    platforms: [
        .macOS("10.15")
    ],
    products: [
        .library(name: "ServerTemplate", targets: ["App"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/MeerkatServerProvider", from: "1.0.0")
    ],
    targets: [
        .target(name: "App", dependencies: ["MeerkatServerProvider"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)
